package edu.epam.izhevsk.junit;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.AdditionalMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)

public class PaymentControllerTest {

    @Mock
    private AccountService accountService;
    @Mock
    private  DepositService depositService;
    @InjectMocks
    private PaymentController paymentController;

    private final long userId = 100L, amount=100L;
    private long rightAmount =55L, wrongAmount = 107L;


    @Before
    public void configure() throws InsufficientFundsException{ // Mock initialisation should be done in one place, for each test

        /*accountService = Mockito.mock(AccountService.class);
        depositService = Mockito.mock(DepositService.class);
        paymentController = new PaymentController(accountService, depositService);*/

        // пропишем логику наших методов
        when(accountService.isUserAuthenticated(userId)).thenReturn(true);
        when(depositService.deposit(lt(amount), anyLong())).thenReturn("Succes"); // второе можно сделать любым, потому что авторизовать ся все-равно может только пользователь 100
        when(depositService.deposit(gt(amount), anyLong())).thenThrow(new InsufficientFundsException());

    }

    // протестируем работу нашей заданой логики
    @Test
    public void TestSuccessfulDeposit() throws InsufficientFundsException{

        paymentController.deposit(rightAmount, userId);
        verify(depositService, times(1)).deposit(rightAmount, userId ); // проверяем был ли один вызов метода с таким параметром из Mock-объекта Deposit Service
    }

    @Test
    public void TestFailedDepositAuth() throws InsufficientFundsException{
        try{
            paymentController.deposit(anyLong(), not(userId)); // тестируем авторизацию любого пользователя не 100
            fail( "Ожидаемое исключение не было выброшено" );
        }
        catch(SecurityException e){
            e.printStackTrace();
        }
    }

    @Test(expected = InsufficientFundsException.class) // исключение ожидается здесь и если оно есть то тест проходит успешно
    public void TestFailedDepositAmount() throws InsufficientFundsException{
        paymentController.deposit(wrongAmount, userId);
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none(); // тесты котторые не выбрасывают исключеие не будут затронуты, по умолчанию не ожидаем исключение
    @Test
    public void TestFailedDepositAmount1() throws InsufficientFundsException{

        try{
            paymentController.deposit(wrongAmount, userId);
            expectedException.expect(InsufficientFundsException.class); // это сработает, если исключение не выбросится
        }
        catch(InsufficientFundsException e){
            e.printStackTrace();
        }
    }
}
